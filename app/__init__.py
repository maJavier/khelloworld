'''Application entry point'''
import os
from flask import Flask


def _initialize_blueprints(app) -> None:
    '''Register Flask blueprints'''
    from app.routes.views import views
    app.register_blueprint(views)


def create_app() -> Flask:
    '''Create an app by initializing components'''
    app = Flask(__name__)
    _initialize_blueprints(app)
    return app