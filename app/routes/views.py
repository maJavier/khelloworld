from flask import Blueprint, render_template
import os 

views = Blueprint('views', __name__)

@views.route("/")
def index():
    try:
        ENV_AGENT_HOST = os.environ['AGENT_HOST']
        ENV_MY_POD_IP = os.environ['MY_POD_IP']
        return render_template("index.html", AGENT_HOST=ENV_AGENT_HOST,
                                            MY_POD_IP=ENV_MY_POD_IP)
    except:
        return render_template("index.html")
