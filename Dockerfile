#Pulling base alpine image from dockerhub
FROM alpine:3.15.0

# Installing python
RUN apk update && apk add --no-cache python3 py3-pip

# Create app directory as working directory
WORKDIR /app

# Copy requirements to the working directory
COPY ./requirements.txt /app/requirements.txt

# Installing dependecies
RUN pip install -r requirements.txt

# Copy code from root repo to the working directory
COPY . /app

# python3 run.py
ENTRYPOINT [ "python3" ]

CMD [ "wsgi.py" ]
