### build script ###
docker build -t ${NAME} .
docker tag ${NAME} ${REGISTRY}/${NAME}:${TAG}
docker push ${REGISTRY}/${NAME}:${TAG}