# Thirdpartytrust Challenge

Create a Hello World site hosted on kubernetes, and send the deliverables via email or git.
Please use comments (references are welcomed ) in the yaml files to explain your decisions.
Use any programing language you are familiar with.

# URL
web hosted at:

``` bash
http://third-lb-112110656.us-east-1.elb.amazonaws.com/
```

## Installation

Use git to clone the project

```bash
git clone https://gitlab.com/maJavier/khelloworld.git
```

## Docker build, run and push

```bash
# Build the project locally
docker build -t khelloworld:latest .

# Run the project locally
docker run -d --network host --name khelloworld khelloworld:latest

# Remove the project locally
docker rm -f khelloworld

# Push to registry
./build.sh
```

## Kubernetes 

```bash
# Run the app in your cluster
kubectl apply -f /deploy/staging/

# Remove all
kubectl delete -f /deploy/staging/
```

## Owner
```bash
# email
javieraguirre1697@gmail.com

# LinkedIn
https://www.linkedin.com/in/majavier-agui/
```